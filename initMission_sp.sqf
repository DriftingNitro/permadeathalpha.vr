/* ----------------------------------------------------------------------------

	File: initMission.sqf
	Author: DriftingNitro
	
Description:
    script started when globally activated the mission starter scriptDone
	removes spawner actions and creates a countdown countdown clock
	calls the timer script when initial countdown is complete
	moves the player and allows damage


Parameter(s):
	- Player : Object

Return:
    - none
---------------------------------------------------------------------------- */

deleteVehicle arrowStarter;
removeAllActions vehicleSpawner_0;
removeAllActions vehicleSpawner_1;
removeAllActions vehicleSpawner_2;
removeAllActions vehicleSpawner_3;
removeAllActions vehicleSpawner_4;
removeAllActions vehicleSpawner_5;
removeAllActions vehicleSpawner_6;
//removeAllActions SpawnerSign;
removeAllActions MissionStarter;

INIT_TIME = 60;//When mission should end in seconds
LoadoutPreload = INIT_TIME - 50;
AreaPreload = INIT_TIME - 30;
Countdown_Warning10 = INIT_TIME - 10;
Countdown_Warning9 = INIT_TIME - 9;
Countdown_Warning8 = INIT_TIME - 8;
Countdown_Warning7 = INIT_TIME - 7;
Countdown_Warning6 = INIT_TIME - 6;
Countdown_Warning5 = INIT_TIME - 5;
Countdown_Warning4 = INIT_TIME - 4;
Countdown_Warning3 = INIT_TIME - 3;
Countdown_Warning2 = INIT_TIME - 2;
Countdown_Warning1 = INIT_TIME - 1;
diag_log format ["|initMisison|--!-- countdown timer set %1",INIT_TIME];

if (hasInterface) then
{
diag_log format ["|initMisison|--!-- should be running local and isdedicated false %1",isDedicated];
	disableSerialization;
	1 cutRsc ["H8erHUD","PLAIN"];
	diag_log "|initMisison|--!-- openining timer RscUI";
	waitUntil {!isNull (uiNameSpace getVariable "H8erHUD")};
	_display = uiNameSpace getVariable "H8erHUD";
	_setText = _display displayCtrl 1001;
	_setText ctrlSetBackgroundColor [0,0,0,0];
		_elapsed_time  = 0;
		while {LoadoutPreload > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Not_Clear_F";
		beginPreload = true;

    	"infoNotification" cutRsc ["default","PLAIN"];
        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked2= "Applying loadouts";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display2 = uiNameSpace getVariable "infoNotification";
            _setText2 = _display2 displayCtrl 1012;
            _setText2 ctrlSetStructuredText (parseText format ["%1",_stringWorked2]);
            _setText2 ctrlSetBackgroundColor [0,0,0,0];

		while {AreaPreload > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Not_Clear_F";

		//preloading the start location
		preloadCamera markerPos "west_start";
    	"infoNotification" cutRsc ["default","PLAIN"];
        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked3= "Preloading spawn location";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display2 = uiNameSpace getVariable "infoNotification";
            _setText2 = _display2 displayCtrl 1012;
            _setText2 ctrlSetStructuredText (parseText format ["%1",_stringWorked3]);
            _setText2 ctrlSetBackgroundColor [0,0,0,0];
		
		while {Countdown_Warning10 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";

		_setText ctrlSetTextColor [1, 0, 0, 1];
		
		while {Countdown_Warning9 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning8 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning7 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning6 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning5 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning4 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning3 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning2 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {Countdown_Warning1 > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_CP_Clear_F";
		while {INIT_TIME > _elapsed_time} do 
		{
			_elapsed_time = diag_tickTime - initTime;
			_setText ctrlSetStructuredText parseText format["<t align='left'>Mission was activated, starting in: %1</t>",[(INIT_TIME - _elapsed_time),"SS.MS"] call BIS_fnc_secondsToString];
		};
		playsound "FD_Start_F";

	1 cutRsc ["default","PLAIN"];
} else {
	_elapsed_time  = 0;
	initTime = diag_tickTime;
	while {Countdown_Warning > _elapsed_time} do 
	{
		_elapsed_time = diag_tickTime - initTime;
	};
	while {INIT_TIME > _elapsed_time} do 
	{
		_elapsed_time = diag_tickTime - initTime;
	};
};
cutText  ["", "BLACK OUT"];
sleep 1;
diag_log "|initMisison|--!-- Teleporting the units to the start position";
player setDir random 360;
player setVehiclePosition [getMarkerPos "west_start", [], 0, "none"];
cutText  ["", "BLACK IN"];
player allowDamage true;
playMusic "Intro";
initMission_done = true;

setPlayerRespawnTime 999999;

if(isServer) then {
	diag_log "|initMisison|--!-- server remotely executing the timer script to all clients and itself";
	startTime = diag_tickTime;
	publicVariable "startTime";
	if (isMultiplayer) then {
		[[],"timer.sqf"] remoteExec ["execVM",0,false];
	}
	else
	{
		[[],"timer_SP.sqf"] remoteExec ["execVM",0,false];
	};
	call bomb_fnc_generatePasscodeServer;
};

if(hasInterface) then {
	"infoNotification" cutRsc ["default","PLAIN"];
	"infoNotification" cutRsc ["infoNotification","PLAIN"];
	_stringWorked4= "Eliminate All Hostiles or Secure the Objective";
	waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
		_display2 = uiNameSpace getVariable "infoNotification";
		_setText2 = _display2 displayCtrl 1012;
		_setText2 ctrlSetStructuredText (parseText format ["%1",_stringWorked4]);
		_setText2 ctrlSetBackgroundColor [0,0,0,0];
};