/* ----------------------------------------------------------------------------

	File: LastMan.sqf
	Author: DriftingNitro
	
Description:
    Runs the last man script and music

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

waitUntil {initMission_done};
if (LastOne_notif) exitWith {diag_log "|LastMan|--!-- Last man already running";};
LastOne_notif = true;
playMusic "FinalStand";
sleep 2;
Command sideRadio "LastOne";
["Command", "You're the last one, complete the mission!"] call BIS_fnc_showSubtitle;
diag_log "|LastMan|--!--Notification for last player remaining ran";