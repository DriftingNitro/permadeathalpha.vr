/* ----------------------------------------------------------------------------

	File: LastOne.sqf
	Author: DriftingNitro
	
Description:
    Runs the last enemy script

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

waitUntil {initMission_done};
if (LastMan_notif) exitWith {diag_log "|LastOne|--!-- Last man already running";};
LastMan_notif = true;
Command sideRadio "OneLeft";
["Command", "Only one enemy left!"] call BIS_fnc_showSubtitle;
diag_log "|LastOne|--!--Notification for last enemy remaining ran";