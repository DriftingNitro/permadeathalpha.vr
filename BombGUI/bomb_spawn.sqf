/* ----------------------------------------------------------------------------

	File: bomb_spawn.sqf
	Author: DriftingNitro
	
Description:
    Spawns the passcode entry menu for the bomb display when opening

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

if (terminalInUse)exitWith{hint"That terminal is already in use";};
if (terminalLocked)exitWith{hint"Terminal is in lockdown";};
terminalInUse = true;
publicVariable "terminalInUse";
playSound3D ["a3\missions_f_beta\data\sounds\firing_drills\target_pop-down_large.wss", terminal];
sleep 0.08;
playSound3D ["a3\missions_f_beta\data\sounds\firing_drills\target_pop-down_large.wss", terminal];
sleep 0.23;
[terminal, 3] call BIS_fnc_DataTerminalAnimate;
sleep 2;
disableSerialization;
    openMap false;
    _handle=createdialog "bomb";
    (findDisplay 9005) displaySetEventHandler ["Unload","openMap [false, false];[terminal, 0] call BIS_fnc_DataTerminalAnimate;terminalInUse = false;publicVariable 'terminalInUse';"];
    ((findDisplay 9005) displayCtrl 1030) ctrlSetStructuredText (parseText "<t color='#960000'>Passcode</t>");

//[terminal, 0] call BIS_fnc_DataTerminalAnimate; [terminal,'Open Terminal','\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa','\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa','_this distance _target < 3','_caller distance _target < 3',{},{},{_handle = execVM 'GUI\bomb_spawn.sqf';},{},[],6,0,false,false] call BIS_fnc_holdActionAdd;