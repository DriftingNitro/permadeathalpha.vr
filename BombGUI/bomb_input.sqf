/* ----------------------------------------------------------------------------

	File: bomb_input.sqf
	Author: DriftingNitro
	
Description:
    Enters an integer into the passcode display

Parameter(s):
	- [0] Integer : Number (0-9)

Return:
    - none
---------------------------------------------------------------------------- */

disableSerialization;
// _string select [0, 1]; //selecct first character 0 of length 1
_input = _this select 0;
_string = (ctrlText 1030);

if (_string=="Passcode") then {
    ((findDisplay 9005) displayCtrl 1030) ctrlSetText "";
};

if (_input < 10) exitWith {
     playSound "beep1";
    _string = (ctrlText 1030);
    private _number = str _input;
    _string = _string + _number;

    ((findDisplay 9005) displayCtrl 1030) ctrlSetText _string;
};

switch (_input) do {
    case 10: {((findDisplay 9005) displayCtrl 1030) ctrlSetText ""; playSound "beep2";};
    case 11: {_string = (ctrlText 1030); _handle =[_string] execVM "BombGUI\bomb_check.sqf";  playSound "beep3";};
    default {hint "invalid input";};
};