/* ----------------------------------------------------------------------------

	File: bomb_check.sqf
	Author: DriftingNitro
	
Description:
    Checks if the entered passcode is correct and locksdown the terminal if not correct.

Parameter(s):
	- [0] String : Passcode

Return:
    - none
---------------------------------------------------------------------------- */

private _entered = _this select 0;
if (_entered == passcode) exitwith {
    closeDialog 0;
    [terminal,0] call BIS_fnc_holdActionRemove;
    [[],"Bluforvictory_defused.sqf"] remoteExec ["execVM",0,true];
    bombDefused = true;
    publicVariable "bombDefused";
    ["TaskSucceeded",["","Objective Secure"]] call BIS_fnc_showNotification;
    playSound3D ["A3\Missions_F_Bootcamp\data\sounds\vr_shutdown.wss", terminal, false, getPosASL terminal, 20,1,100];
    };
closeDialog 0;
playSound3D ["a3\missions_f_beta\data\sounds\firing_drills\course_active.wss", terminal];
terminalLocked = true;
publicVariable "terminalLocked";
[terminal, "red","red","red"] call BIS_fnc_DataTerminalColor;
["IntelAdded",["Terminal on Lockdown (20 Seconds)","\A3\Ui_f\data\GUI\Rsc\RscDisplayDynamicGroups\Lock.paa"]] call BIS_fnc_showNotification;
playSound3D ["A3\Sounds_F_Bootcamp\SFX\VR\Simulation_Fatal.wss", terminal, false, getPosASL terminal, 20,1,100];
sleep 20;
[terminal, "blue","blue","orange"] call BIS_fnc_DataTerminalColor;

terminalLocked = false;
publicVariable "terminalLocked";
playSound3D ["A3\Sounds_F_Bootcamp\SFX\VR\Simulation_Restart.wss", terminal, false, getPosASL terminal, 20,1,100];