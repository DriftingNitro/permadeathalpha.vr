/* ----------------------------------------------------------------------------

	File: initPlayerLocal.sqf
	Author: DriftingNitro
	
Description:
    runs locally on the machine when joining the server and entering the game, 
    checks if the unit is zeus, if so checks if the mission has started, 
    if not then opens the gamemaster menu


Parameter(s):
	- Player : object

Return:
    - none
---------------------------------------------------------------------------- */

sleep 0.2;


//
//FPS DEBUGGING SCRIPT FOR ZEUS
//
[] spawn {
   while {true} do {
        player setVariable ["DNI_PlayerFPS", floor diag_fps, true];
        sleep 0.1;
    };
};

removeAllWeapons player; 
removeVest player; 
removeBackpack player; 
removeHeadgear player; 
if (player == z1) then {
    diag_log "|initLocal|--!--Player is zeus";
    if(!MissionActive) then {
            diag_log "|initLocal|--!--Mission inactive Player confirmed as main zeus, spawning menu";
            _handle=createdialog "tools";
            diag_log "|initLocal|--!-- spawn main menu diolag";
            (findDisplay 9002) displaySetEventHandler ["KeyDown","if((_this select 1)isEqualTo 1) then {true}"];        //Disable ESC
            diag_log "|initLocal|--!-- esc button disabled";
    } else {
        [true,true] call BIS_fnc_forceCuratorInterface;
        diag_log "|initLocal|--!--forcing interface because mission is already started";
    };
} else {
    if(MissionActive) then {
        diag_log "|initLocal|--!--mission already active and not zeus, starting spectator";
        ["Initialize", 
        [player, //unit being initialized
        [West], //sides that can be spectated
        false, //can AI be viewed
        false, //can free camera be used
        false, //can 3rd person be used
        false, //show focus info widgets
        false, //show camera button widgets or not
        true, //Show controls helper widget
        true, //whether to show header widget
        true //Whether to show entities/locations lists
        ]] call BIS_fnc_EGSpectator;
    };
    
};