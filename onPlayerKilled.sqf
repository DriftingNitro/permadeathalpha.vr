/* ----------------------------------------------------------------------------

	File: onPlayerKilled.sqf
	Author: DriftingNitro
	
Description:
    Activates the spectator mode and removes the death blur
    
Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

diag_log "|onKilled|--!--calling spectator";
["Initialize", 
[player, //unit being initialized
[West], //sides that can be spectated
false, //can AI be viewed
false, //can free camera be used
false, //can 3rd person be used
false, //show focus info widgets
false, //show camera button widgets or not
true, //Show controls helper widget
true, //whether to show header widget
true //Whether to show entities/locations lists
]] call BIS_fnc_EGSpectator;
sleep 3;
BIS_DeathBlur ppEffectAdjust [0.0];
BIS_DeathBlur ppEffectCommit 0.0;