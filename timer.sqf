/* ----------------------------------------------------------------------------

	File: timer.sqf
	Author: DriftingNitro
	
Description:
    activates the timer display and fails the mission when time is up

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

Timer_Length = parseNumber END_TIME;//When mission should end in seconds
Timer_Warning10 = Timer_Length - 10;
Timer_Warning9 = Timer_Length - 9;
Timer_Warning8 = Timer_Length - 8;
Timer_Warning7 = Timer_Length - 7;
Timer_Warning6 = Timer_Length - 6;
Timer_Warning5 = Timer_Length - 5;
Timer_Warning4 = Timer_Length - 4;
Timer_Warning3 = Timer_Length - 3;
Timer_Warning2 = Timer_Length - 2;
Timer_Warning1 = Timer_Length - 1;
_elapsed_time  = 0;//will change to serverTime when working
diag_log format ["|Timer|--!-- %1 is the time limit",Timer_Length];

if (isDedicated) then {
	while {Timer_Length > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;//will change to serverTime when working
	};
	if (bombDefused) exitWith {};
	[[],"BluforFailure_time.sqf"] remoteExec ["execVM",0,true];
};

if (hasInterface) then
{
	disableSerialization;
	2 cutRsc ["NitroInfo","PLAIN"];
	waitUntil {!isNull (uiNameSpace getVariable "NitroInfo")};
	_display = uiNameSpace getVariable "NitroInfo";
	_setText = _display displayCtrl 1010;
	_setText ctrlSetBackgroundColor [0,0,0,0];
	while {Timer_Warning10 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	_setText ctrlSetTextColor [1, 0, 0, 1];
	playsound "FD_CP_Clear_F";
	while {Timer_Warning9 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning8 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning7 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning6 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning5 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning4 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning3 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning2 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Warning1 > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	playsound "FD_CP_Clear_F";
	while {Timer_Length > _elapsed_time} do 
	{
		_elapsed_time = serverTime - startTime;
		_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];
		if(bombDefused) exitWith {_setText ctrlSetText format["%1",[(Timer_Length - _elapsed_time),"MM:SS.MS"] call BIS_fnc_secondsToString];};
	};
	if(!bombDefused) then {
	_setText ctrlSetText "00:00.0000";
	};
	diag_log "|Timer|--!--set display text to 0";
	playsound "FD_Start_F";
	//2 cutRsc ["default","PLAIN"];
	diag_log "|Timer|--!--Timer ended calling mission failure";
	if (isServer) then {
		if (bombDefused) exitWith {};
		[[],"BluforFailure_time.sqf"] remoteExec ["execVM",0,true];
	};
};