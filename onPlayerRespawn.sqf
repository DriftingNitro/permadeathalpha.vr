/* ----------------------------------------------------------------------------

	File: onPlayerRespawn.sqf
	Author: DriftingNitro
	
Description:
    termiantes the spectator mode

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

["Terminate"] call BIS_fnc_EGSpectator;
diag_log "|onRespawn|--!--terminating spectator";