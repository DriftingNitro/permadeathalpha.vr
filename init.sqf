if ((!isServer) && (player != player)) then
{
  waitUntil {player == player};
};
diag_log "|INIT|--!--STARTING MISSION, SERVER ACTIVE OR PLAYER = PLAYER";
diag_log "/////////////////////////////////////////////////////////////////////////////////////////////////////////";
diag_log "/////////////////////////////////////////////////////////////////////////////////////////////////////////";
diag_log "/////////////////////////////////////////////////////////////////////////////////////////////////////////";
diag_log "__/\\\\\\\\\\\\\____/\\\\\\\\\\\\\\\____/\\\\\\\\\______/\\\\____________/\\\\_____/\\\\\\\\\____        ";
diag_log " _\/\\\/////////\\\_\/\\\///////////___/\\\///////\\\___\/\\\\\\________/\\\\\\___/\\\\\\\\\\\\\__       ";
diag_log "  _\/\\\_______\/\\\_\/\\\_____________\/\\\_____\/\\\___\/\\\//\\\____/\\\//\\\__/\\\/////////\\\_      ";
diag_log "   _\/\\\\\\\\\\\\\/__\/\\\\\\\\\\\_____\/\\\\\\\\\\\/____\/\\\\///\\\/\\\/_\/\\\_\/\\\_______\/\\\_     ";
diag_log "    _\/\\\/////////____\/\\\///////______\/\\\//////\\\____\/\\\__\///\\\/___\/\\\_\/\\\\\\\\\\\\\\\_    ";
diag_log "     _\/\\\_____________\/\\\_____________\/\\\____\//\\\___\/\\\____\///_____\/\\\_\/\\\/////////\\\_   ";
diag_log "      _\/\\\_____________\/\\\_____________\/\\\_____\//\\\__\/\\\_____________\/\\\_\/\\\_______\/\\\_  ";
diag_log "       _\/\\\_____________\/\\\\\\\\\\\\\\\_\/\\\______\//\\\_\/\\\_____________\/\\\_\/\\\_______\/\\\_ ";
diag_log "        _\///______________\///////////////__\///________\///__\///______________\///__\///________\///__";
diag_log "__/\\\\\\\\\\\\_____/\\\\\\\\\\\\\\\_____/\\\\\\\\\_____/\\\\\\\\\\\\\\\__/\\\________/\\\_              ";
diag_log " _\/\\\////////\\\__\/\\\///////////____/\\\\\\\\\\\\\__\///////\\\/////__\/\\\_______\/\\\_             ";
diag_log "  _\/\\\______\//\\\_\/\\\______________/\\\/////////\\\_______\/\\\_______\/\\\_______\/\\\_            ";
diag_log "   _\/\\\_______\/\\\_\/\\\\\\\\\\\_____\/\\\_______\/\\\_______\/\\\_______\/\\\\\\\\\\\\\\\_           ";
diag_log "    _\/\\\_______\/\\\_\/\\\///////______\/\\\\\\\\\\\\\\\_______\/\\\_______\/\\\/////////\\\_          ";
diag_log "     _\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_______\/\\\_______\/\\\_______\/\\\_         ";
diag_log "      _\/\\\_______/\\\__\/\\\_____________\/\\\_______\/\\\_______\/\\\_______\/\\\_______\/\\\_        ";
diag_log "       _\/\\\\\\\\\\\\/___\/\\\\\\\\\\\\\\\_\/\\\_______\/\\\_______\/\\\_______\/\\\_______\/\\\_       ";
diag_log "        _\////////////_____\///////////////__\///________\///________\///________\///________\///__      ";
diag_log "/////////////////////////////////////////////////////////////////////////////////////////////////////////";
diag_log format ["////////////////////////////Made by DriftingNitro : %1////////////////////////////",missionName];
diag_log "/////////////////////////////////////////////////////////////////////////////////////////////////////////";

diag_log "|INIT|--!--INITIALIZING";
tf_no_auto_long_range_radio = true;
enableSaving [false, false];

if (hasinterface) then {
	waitUntil {!isnull player};
	player enableStamina false;
	player forceWalk false;
	player setCustomAimCoef 0.5;
	player setUnitRecoilCoefficient 1.6;
	player addEventHandler ["Respawn", {
		player enableStamina false;
		player forceWalk false;
		player setCustomAimCoef 0.5;
		player setUnitRecoilCoefficient 0.3;		
	}];
};


diag_log "|INIT|--!--changing color of terminal";
[terminal, "blue","blue","orange"] call BIS_fnc_DataTerminalColor;

diag_log "|INIT|--!--applying terminal hold action";
[
terminal,
"Open Terminal",
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
"_this distance _target < 3",
"_caller distance _target < 3",
{if (terminalInUse)exitWith{hint"Somebody is already using this terminal";};},
{if (terminalInUse)exitWith{hint"Somebody is already using this terminal";};},
{_handle = execVM "BombGUI\bomb_spawn.sqf";},
{},
[],
4,
12,
false,
false
] call BIS_fnc_holdActionAdd;

terminal attachTo [tower, [-3.2,-3.0,2.467]];
terminal setdir 0;
zMod1 addCuratorEditableObjects [[tower],true ];

diag_log "|INIT|--!--initalizing booleans";
MissionActive = false;
EndScreen = false;
VictoryPlaying = false;
LastMan_notif=false;
LastOne_notif=false;
creatingVehicle = false;
beginPreload = false;
initMission_done = false;
bombDefused = false;
terminalInUse = false; 
terminalLocked = false; 
passcodeGenerated = false;
startTime = 0;

enteredEast = false;
enteredWest = false;
enteredObj = false;
enteredTime = false;

END_TIME = "300"; //When mission should end in seconds. Data type string; Default
diag_log format ["|INIT|--!-- Default end time set to %1",END_TIME];

//
//FPS DEBUGGING SCRIPT FOR ZEUS
//
addMissionEventHandler ["Draw3D", {
	{
		_distance = position curatorCamera distance _x;
		if (_distance < 1200) then {
			_playerFPS = _x getVariable ["DNI_PlayerFPS",50];
			if (_playerFPS  <20) then 
			{
				drawIcon3D["",[1,0,0,0.7],position _x,1,2,0,format["%1 FPS: %2", name _x, str _playerFPS],1,0.05, "PuristaMedium","center"];
			}
			else
			{
				drawIcon3D["",[1,1,1,0.3],position _x,1,1,0,format["%1 FPS: %2", name _x, str _playerFPS],0,0.03,"PuristaMedium","center"];
			};
		};
	} forEach allPlayers;
}];