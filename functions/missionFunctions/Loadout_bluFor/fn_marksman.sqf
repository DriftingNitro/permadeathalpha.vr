/* ----------------------------------------------------------------------------
Function: blu_fnc_marksman

	File: fn_marksman.sqf
	Author: DriftingNitro
	
Description:
    Takes the unit object and applies the loadout to them

Parameter(s):
	-  [0] Unit (Infantry) : Object

Return:
    - none
---------------------------------------------------------------------------- */

private _unit = _this select 0;

comment "Marksman Unit";

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "Gen3_Tan";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_IR_Strobe_Item";
_unit addItemToUniform "ACE_Flashlight_XL50";
_unit addItemToUniform "ACE_MapTools";
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_tourniquet";};
_unit addItemToUniform "ACE_morphine";
_unit addItemToUniform "ACE_epinephrine";
for "_i" from 1 to 10 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_CableTie";};
_unit addItemToUniform "SmokeShellBlue";
_unit addItemToUniform "SmokeShellRed";
_unit addItemToUniform "CUP_15Rnd_9x19_M9";
_unit addVest "lbt_comms_coy";
_unit addItemToVest "ACE_RangeCard";
_unit addItemToVest "ACE_Kestrel4500";
for "_i" from 1 to 4 do {_unit addItemToVest "SmokeShell";};
for "_i" from 1 to 4 do {_unit addItemToVest "CUP_20Rnd_762x51_B_M110";};
for "_i" from 1 to 2 do {_unit addItemToVest "CUP_20Rnd_TE1_White_Tracer_762x51_M110";};
_unit addHeadgear "rhsusf_mich_bare_tan";

comment "Add weapons";
_unit addWeapon "CUP_srifle_M110";
_unit addPrimaryWeaponItem "SMA_ANPEQ15_TAN";
_unit addPrimaryWeaponItem "RH_shortdot";
_unit addPrimaryWeaponItem "RH_TD_ACB";
_unit addWeapon "CUP_hgun_M9";
_unit addWeapon "ACE_VectorDay";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152";
_unit linkItem "ACE_NVG_Gen4";