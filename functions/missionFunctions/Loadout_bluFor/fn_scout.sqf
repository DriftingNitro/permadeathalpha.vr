/* ----------------------------------------------------------------------------
Function: blu_fnc_scout

	File: fn_scout.sqf
	Author: DriftingNitro
	
Description:
    Takes the unit object and applies the loadout to them

Parameter(s):
	-  [0] Unit (Infantry) : Object

Return:
    - none
---------------------------------------------------------------------------- */

private _unit = _this select 0;

comment "Scout Unit";

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "Gen3_Tan";
_unit addItemToUniform "ACE_EarPlugs";
_unit addItemToUniform "ACE_IR_Strobe_Item";
_unit addItemToUniform "ACE_Flashlight_XL50";
_unit addItemToUniform "ACE_MapTools";
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_tourniquet";};
_unit addItemToUniform "ACE_morphine";
_unit addItemToUniform "ACE_epinephrine";
for "_i" from 1 to 10 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 2 do {_unit addItemToUniform "ACE_CableTie";};
_unit addItemToUniform "CUP_15Rnd_9x19_M9";
_unit addItemToUniform "HandGrenade";
_unit addItemToUniform "ACE_M84";
_unit addVest "lbt_operator_coy";
for "_i" from 1 to 8 do {_unit addItemToVest "30Rnd_556x45_Stanag";};
for "_i" from 1 to 4 do {_unit addItemToVest "SmokeShell";};
for "_i" from 1 to 3 do {_unit addItemToVest "HandGrenade";};
for "_i" from 1 to 2 do {_unit addItemToVest "ACE_M84";};
_unit addHeadgear "rhsusf_mich_bare_tan";

comment "Add weapons";
_unit addWeapon "SMA_MK18MOEBLK";
_unit addPrimaryWeaponItem "RH_barska_rds";
_unit addWeapon "CUP_hgun_M9";
_unit addWeapon "Rangefinder";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152_1";
_unit linkItem "ACE_NVG_Gen4";