// Leader
displayName = "Squad Leader";
icon = "\A3\Ui_f\data\GUI\Cfg\Ranks\colonel_gs.paa";
uniform = "Gen3_Tan";
backpack = "";
weapons[] = {"SMA_MK18MOEBLK","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","CUP_15Rnd_9x19_M9","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","SmokeShell","SmokeShell","SmokeShell","SmokeShell","HandGrenade","HandGrenade","ACE_M84","ACE_M84"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_norotos_arc_alt_tan_headset","","ItemMap","ItemCompass","ItemWatch","tf_anprc152","ItemGPS","ACE_NVG_Gen4"};

// UAV
uniform = "Gen3_Tan";
backpack = "B_UAV_01_backpack_F";
weapons[] = {"SMA_MK18MOEBLK","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","CUP_15Rnd_9x19_M9","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","SmokeShell","SmokeShell","SmokeShell","SmokeShell","HandGrenade","HandGrenade","ACE_M84","ACE_M84"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152_11","B_UavTerminal","ACE_NVG_Gen4"};

// Medic
uniform = "Gen3_Tan";
backpack = "TRYK_B_Medbag";
weapons[] = {"SMA_MK18MOEBLK","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","CUP_15Rnd_9x19_M9","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","SmokeShell","SmokeShell","SmokeShell","SmokeShell","HandGrenade","HandGrenade","ACE_M84","ACE_M84","SmokeShell","SmokeShell","SmokeShell","SmokeShell","SmokeShell","SmokeShell","SmokeShell","SmokeShell"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie","ACE_surgicalKit","ACE_personalAidKit","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_morphine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_salineIV","ACE_salineIV","ACE_salineIV","ACE_salineIV","ACE_salineIV_500","ACE_salineIV_500","ACE_salineIV_500","ACE_salineIV_500"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152","ACE_NVG_Gen4"};

// FTL
uniform = "Gen3_Tan";
backpack = "";
weapons[] = {"SMA_MK18TANBLK_GL","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","murshun_cigs_lighter","CUP_15Rnd_9x19_M9","1Rnd_Smoke_Grenade_shell","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","SmokeShell","SmokeShell","SmokeShell","SmokeShell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","murshun_cigs_cigpack","murshun_cigs_cigpack","1Rnd_Smoke_Grenade_shell"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_weapons_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152_9","ACE_NVG_Gen4"};

// Autorifleman
uniform = "Gen3_Tan";
backpack = "B_AssaultPack_cbr";
weapons[] = {"sma_minimi_mk3_762tlb","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","murshun_cigs_cigpack","murshun_cigs_lighter","CUP_15Rnd_9x19_M9","SmokeShell","SmokeShell","SmokeShell","SmokeShell","150Rnd_762x54_Box","150Rnd_762x54_Box","150Rnd_762x54_Box","150Rnd_762x54_Box","HandGrenade","HandGrenade"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie","ACE_EntrenchingTool","ACE_SpraypaintBlack"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152_2","ItemGPS","ACE_NVG_Gen4"};

// Asst. Autorifleman
uniform = "Gen3_Tan";
backpack = "B_AssaultPack_cbr";
weapons[] = {"SMA_MK18MOEBLK","CUP_launch_M136","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","CUP_15Rnd_9x19_M9","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","SmokeShell","SmokeShell","SmokeShell","SmokeShell","HandGrenade","HandGrenade","HandGrenade","ACE_M84","ACE_M84","150Rnd_762x54_Box","150Rnd_762x54_Box","150Rnd_762x54_Box","HandGrenade","ACE_PreloadedMissileDummy_CUP"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152","ACE_NVG_Gen4"};

// Grenadier
uniform = "Gen3_Tan";
backpack = "B_AssaultPack_cbr";
weapons[] = {"SMA_MK18TANBLK_GL","CUP_hgun_M9","Binocular","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","murshun_cigs_lighter","CUP_15Rnd_9x19_M9","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","SmokeShell","SmokeShell","SmokeShell","SmokeShell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","murshun_cigs_cigpack","murshun_cigs_cigpack","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_Smoke_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F","UGL_FlareRed_F"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_weapons_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152","ACE_NVG_Gen4"};

// Marksman
uniform = "Gen3_Tan";
backpack = "";
weapons[] = {"CUP_srifle_M110","CUP_hgun_M9","ACE_VectorDay","Throw","Put"};
magazines[] = {"SmokeShellBlue","SmokeShellRed","CUP_15Rnd_9x19_M9","SmokeShell","SmokeShell","SmokeShell","SmokeShell","CUP_20Rnd_762x51_B_M110","CUP_20Rnd_762x51_B_M110","CUP_20Rnd_762x51_B_M110","CUP_20Rnd_762x51_B_M110","CUP_20Rnd_TE1_White_Tracer_762x51_M110","CUP_20Rnd_TE1_White_Tracer_762x51_M110"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie","ACE_RangeCard","ACE_Kestrel4500"};
linkedItems[] = {"lbt_comms_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152","ACE_NVG_Gen4"};

// Scout
uniform = "Gen3_Tan";
backpack = "";
weapons[] = {"SMA_MK18MOEBLK","CUP_hgun_M9","Rangefinder","Throw","Put"};
magazines[] = {"CUP_15Rnd_9x19_M9","HandGrenade","ACE_M84","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_Stanag","SmokeShell","SmokeShell","SmokeShell","SmokeShell","HandGrenade","HandGrenade","HandGrenade","ACE_M84","ACE_M84"};
items[] = {"ACE_EarPlugs","ACE_IR_Strobe_Item","ACE_Flashlight_XL50","ACE_MapTools","ACE_tourniquet","ACE_tourniquet","ACE_morphine","ACE_epinephrine","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_elasticBandage","ACE_CableTie","ACE_CableTie"};
linkedItems[] = {"lbt_operator_coy","rhsusf_mich_bare_tan","","ItemMap","ItemCompass","ItemWatch","tf_anprc152_1","ACE_NVG_Gen4"};