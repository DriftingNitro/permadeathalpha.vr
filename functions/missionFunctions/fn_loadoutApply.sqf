/* ----------------------------------------------------------------------------
Function: missionStarter_fnc_loadoutApply

	File: fn_loadoutApply.sqf
	Author: DriftingNitro
	
Description:
	Takes the unit and applies a loadout based on it's classname from searching a switch:case library.
	
Parameter(s):
	- Unit : object

Return:
    none
---------------------------------------------------------------------------- */

private _unit = _this;

    diag_log format ["|loadoutApply|--!--attempting to set loadout for unit: %1",_unit];
	_classname = toLower (typeOf _this);
    diag_log format ["|loadoutApply|--!--attempting to set loadout for type: %1",_classname];

	//Blufor functions
    if (side group _this == west) exitWith {
        diag_log format ["|loadoutApply|--!--Side determined to be west for %1",_classname];
        switch (_classname) do {
            case "b_survivor_f": {};
            case "b_officer_f": {[_this] call blu_fnc_squadLead;};
            case "b_soldier_sl_f": {[_this] call blu_fnc_squadLead;};
            case "b_soldier_tl_f": {[_this] call  blu_fnc_teamLead;};
            case "b_soldier_uav_f": {[_this] call  blu_fnc_uavOp;};
            case "b_medic_f": {[_this] call  blu_fnc_medic;};
            case "b_soldier_ar_f": {[_this] call  blu_fnc_autoRifleman;};
            case "b_soldier_aar_f": {[_this] call  blu_fnc_asstAutoRifleman;};
            case "b_soldier_gl_f": {[_this] call  blu_fnc_grenadier;};
            case "b_soldier_m_f": {[_this] call  blu_fnc_marksman;};
            case "b_soldier_lite_f": {[_this] call  blu_fnc_scout;};
            case "empty_classname": {[_this] call  blu_fnc_scout;};
            case "empty_classname": {[_this] call  blu_fnc_scout;};
            case "empty_classname": {[_this] call  blu_fnc_scout;};
            case "empty_classname": {[_this] call  blu_fnc_scout;};
            default {_error = format ["Classname not valid blufor: %1",_x]; systemChat _error; hint _error;};
        /* Replace "empty_classname" with the classname of the unit you want to have this loadout applied to, and replace "blu_fnc_scout" with the loadout functino you want be applied to it */
        };
    };

	//Opfor functions
     if (side group _this == east) exitWith {
        diag_log format ["|loadoutApply|--!--Side determined to be east for %1",_classname];
        switch (_classname) do {
            case "o_survivor_f": {};
            case "o_officer_f": {[_this] call op_fnc_squadLead;};
            case "o_soldier_sl_f": {[_this] call op_fnc_squadLead;};
            case "o_soldier_tl_f": {[_this] call  op_fnc_teamLead;};
            case "o_soldier_uav_f": {[_this] call  op_fnc_uavOp;};
            case "o_medic_f": {[_this] call  op_fnc_medic;};
            case "o_soldier_ar_f": {[_this] call  op_fnc_autoRifleman;};
            case "o_soldier_aar_f": {[_this] call  op_fnc_asstAutoRifleman;};
            case "o_soldier_gl_f": {[_this] call  op_fnc_grenadier;};
            case "o_soldier_m_f": {[_this] call  op_fnc_marksman;};
            case "o_soldier_lite_f": {[_this] call  op_fnc_scout;};
            case "empty_classname": {[_this] call  op_fnc_scout;};
            case "empty_classname": {[_this] call  op_fnc_scout;};
            case "empty_classname": {[_this] call  op_fnc_scout;};
            case "empty_classname": {[_this] call  op_fnc_scout;};
            default {_error = format ["Classname not valid opfor: %1",_x]; systemChat _error; hint _error;};
        };
    };