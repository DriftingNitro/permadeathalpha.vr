/* ----------------------------------------------------------------------------
Function: missionStarter_fnc_getUnits

	File: fn_getUnits.sqf
	Author: DriftingNitro
	
Description:
	Search all units and compile an array of players
	make the first call to missionStarter_fnc_remainBlufor
	Apply killed event handlers and loadouts for each player unit
	Spawn enemy units based on the number of, and classnames of, the player units
	Create an array of the opfor units spawned
	make the first call to missionStarter_fnc_remainOpfor

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

private ["_bluforUnit","_classname","_LowerClassname","_caseResult","_markerPos","_newUnit"];

BluforPlayers = [];
OpforPlayers = [];

//BluforPlayers = allUnits select {side group _x == west};
BluforPlayers = allUnits select {(side group _x == west) && (typeOf _x != "B_UAV_01_F")};

{
	diag_log format ["|getUnits|--!--Calling loadout function for %1",_x];
	_x remoteExecCall  ["missionStarter_fnc_loadoutApply",_x];
		diag_log format ["|getUnits|--!--Event handler added to %1",_x];
		_x addMPEventHandler ["MPKilled", {
			//BluforPlayers = allUnits select {side group _x == west};
			BluforPlayers = allUnits select {(side group _x == west) && (typeOf _x != "B_UAV_01_F")};
			diag_log format ["|getUnits|--!--BluforPlayers list calculated: %1",BluforPlayers];
			BluforPlayers call missionStarter_fnc_remainBlufor;
			diag_log format ["|getUnits|--!--Calling function for remianing blufor from: %1",_x];
			}];
} forEach BluforPlayers;

diag_log format ["|getUnits|--!--Calling to check remaining blufor of this array: %1",BluforPlayers];
BluforPlayers call missionStarter_fnc_remainBlufor;

{ 	
	diag_log format ["|getUnits|--!--This is the unit: %1",_x];

	_LowerClassname = (toLower (typeOf (_x)));
	_caseResult = switch (_LowerClassname) do {
		case "b_officer_f": {"O_Soldier_SL_F"};
		case "b_soldier_sl_f": {"O_Soldier_SL_F"};
		case "b_soldier_tl_f": {"O_Soldier_TL_F"};
		case "b_soldier_uav_f": {"O_Soldier_UAV_F"};
		case "b_medic_f": {"O_Medic_F"};
		case "b_soldier_ar_f": {"O_Soldier_AR_F"};
		case "b_soldier_aar_f": {"O_Soldier_AAR_F"};
		case "b_soldier_gl_f": {"O_Soldier_GL_F"};
		case "b_soldier_m_f": {"O_Soldier_M_F"};
		case "b_soldier_lite_f": {"O_Soldier_lite_F"};
		default {""};
	};
	diag_log format ["|getUnits|--!--This is the classname: %1",_LowerClassname];
	_caseResult call missionStarter_fnc_createEnemy;
} forEach BluforPlayers;

//OpforPlayers = allUnits select {side group _x == east};
OpforPlayers = allUnits select {(side group _x == east) && (typeOf _x != "O_UAV_01_F")};

diag_log format ["|getUnits|--!--The array of Opfor: %1",OpforPlayers];
diag_log format ["|getUnits|--!--There are this many opfor: %1",count OpforPlayers];

{
	_x remoteExecCall  ["missionStarter_fnc_loadoutApply",_x];
	_pWeap = primaryWeapon _x;
	_x selectWeapon _pWeap;
} forEach OpforPlayers;

diag_log format ["|getUnits|--!--Calling to check remaining opfor of this array: %1",OpforPlayers];
OpforPlayers call missionStarter_fnc_remainOpfor;

_missionMaker = owner z1;
(group OpGrpLead) setGroupOwner _missionMaker;