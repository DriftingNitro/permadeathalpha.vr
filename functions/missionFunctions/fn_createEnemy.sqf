/* ----------------------------------------------------------------------------
Function: missionStarter_fnc_createEnemy

	File: fn_createEnemy.sqf
	Author: DriftingNitro
	
Description:
	Takes the classname of the unit, spawns it, and applies the appropriate event handlers and ai skill settings

Parameter(s):
	- Infantry Unit Classname : string

Return:
    - none
---------------------------------------------------------------------------- */

private ["_newUnit","_markerPos","_tempUnit"];


if (_this == "") exitWith{diag_log format ["|createEnemy|--!--exiting because object is: %1",_this];};
_markerPos = getMarkerPos "East_Start";

diag_log format ["|createEnemy|--!--Creating unit of classname: %1",_this];

_newUnit = (group OpGrpLead) createUnit [_this, getMarkerPos "East_Start", [], 0, "NONE"];

diag_log format ["|createEnemy|--!--Unit created of classname %1 and named %2", _this, _newUnit];

_newUnit disableAI "PATH";
_newUnit setskill ["aimingSpeed",0.6];
_newUnit setskill ["aimingAccuracy",0.1];
_newUnit setskill ["aimingShake",0.1];
_newUnit setskill ["spotDistance",0.8];
_newUnit setskill ["spotTime",1];
_newUnit setskill ["courage",1];
_newUnit setskill ["commanding",1];

diag_log format ["|createEnemy|--!--Skills for %1 set.", _newUnit];

	_newUnit addMPEventHandler ["MPKilled", {
		
		//OpforPlayers = allUnits select {side group _x == east};
		OpforPlayers = allUnits select {(side group _x == east) && (typeOf _x != "O_UAV_01_F")};
		diag_log format ["|getUnits|--!--BluforPlayers list calculated: %1",OpforPlayers];
		OpforPlayers call missionStarter_fnc_remainOpfor;
		diag_log format ["|getUnits|--!--Calling function for remianing blufor from: %1",_x];
		
		}];
	diag_log format ["|createEnemy|--!--Event handler for %1 added.", _newUnit];

zMod1 addCuratorEditableObjects [[_newUnit],true ];
zMod2 addCuratorEditableObjects [[_newUnit],true ];
zMod3 addCuratorEditableObjects [[_newUnit],true ];

diag_log format ["|createEnemy|--!-- %1 added to all 3 zeus modules.", _newUnit];
