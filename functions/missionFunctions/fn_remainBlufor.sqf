/* ----------------------------------------------------------------------------
Function: missionStarter_fnc_remainBlufor

	File: fn_remainBlufor.sqf
	Author: DriftingNitro
	
Description:
	Checks the list and counts the number of alive units, (subtract one for the group leader dummies)
	calls the script to update the blufor remaining diplay counter
	checks the count of alive players and if 1 run the last man standing script
	if there is less than 1, run mission failure script

Parameter(s):
	- List of Units : Array

Return:
    - none
---------------------------------------------------------------------------- */
if(isServer) then {
	disableSerialization;

	diag_log format ["|remainBlufor|--!--Units: %1, number of units in array: %2", _this, count _this];
	diag_log  "|remainBlufor|--!--Checking the alive Blufor units";
	private _BluforAliveCount = {alive _x} count _this;
	diag_log format ["|remainBlufor|--!--Checking if one remaining | %1", _BluforAliveCount];
	private _displayCount = _BluforAliveCount - 1;
	[[_displayCount],"GUI\update_blufor.sqf"] remoteExec ["execVM",0,false];

	if(_BluforAliveCount == 2) exitWith
	{
		diag_log format ["|remainBlufor|--!--count should be 2 for last one | %1", _BluforAliveCount];
		[[],"LastMan.sqf"] remoteExec ["execVM",0,false];
	};

	diag_log format ["|remainBlufor|--!--Checking if all dead | %1", _BluforAliveCount];

	if(_BluforAliveCount == 1) exitWith
	{
		diag_log format ["|remainBlufor|--!--count should be 1 for all dead | %1", _BluforAliveCount];
		[[],"BluforFailure.sqf"] remoteExec ["execVM",0,false];
	};
};