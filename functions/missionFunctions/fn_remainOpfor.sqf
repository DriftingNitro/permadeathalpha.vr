/* ----------------------------------------------------------------------------
Function: missionStarter_fnc_remainOpfor

	File: fn_remainOpfor.sqf
	Author: DriftingNitro
	
Description:
	Checks the list and counts the number of alive units, (subtract one for the group leader dummies)
	calls the script to update the Opfor remaining diplay counter
	checks the count of alive players and if 1 run the last man standing script
	if there is less than 1, run mission failure script

Parameter(s):
	- List of Units : Array

Return:
    - none
---------------------------------------------------------------------------- */
if(isServer) then {
	disableSerialization;

	diag_log format ["|remainOpfor|--!--Units: %1, number of units in array: %2", _this, count _this];
	diag_log  "|remainOpfor|--!--Checking the alive Opfor units";
	private _OpforAliveCount = {alive _x} count _this;
	diag_log format ["|remainOpfor|--!--Checking if one remaining | %1", _OpforAliveCount];
	private _OpDisplayCount = _OpforAliveCount - 1;
	[[_OpDisplayCount],"GUI\update_opfor.sqf"] remoteExec ["execVM",0,false];

	if(_OpforAliveCount == 2) exitWith
	{
		diag_log format ["|remainOpfor|--!--count should be 2 for last one | %1", _OpforAliveCount];
		[[],"LastOne.sqf"] remoteExec ["execVM",0,false];
	};

	diag_log format ["|remainOpfor|--!--Checking if all dead | %1", _OpforAliveCount];

	if(_OpforAliveCount == 1) exitWith
	{
		diag_log format ["|remainOpfor|--!--count should be 1 for all dead | %1", _OpforAliveCount];
		[[],"BluforVictory.sqf"] remoteExec ["execVM",0,false];
	};
};