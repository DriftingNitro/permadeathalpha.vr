_object = _this select 0;

if (typeOf _object == "Land_Laptop_f" ||typeOf _object == "Land_Laptop_unfolded_f") then {
    [
    _object,
    "Search Computer",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
    "_this distance _target < 2",
    "_caller distance _target < 2",
    {/*started*/},
    {_this call bomb_fnc_searchingSound;},
    {_this call bomb_fnc_searchDone;},
    {/*interrupted*/},
    [],
    4,
    12,
    false,
    false
    ] call BIS_fnc_holdActionAdd;
};

if (typeOf _object == "Land_Tabledesk_f" ||typeOf _object == "Land_CampingTable_f") then {
    [
    _object,
    "Search Desk",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
    "_this distance _target < 2",
    "_caller distance _target < 2",
    {/*started*/},
    {_this call bomb_fnc_searchingSound;},
    {_this call bomb_fnc_searchDone;},
    {/*interrupted*/},
    [],
    4,
    12,
    false,
    false
    ] call BIS_fnc_holdActionAdd;
};

if (typeOf _object == "Land_officeCabinet_01_f") then {
    [
    _object,
    "Search Shelf",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
    "_this distance _target < 2",
    "_caller distance _target < 2",
    {/*started*/},
    {_this call bomb_fnc_searchingSound;},
    {_this call bomb_fnc_searchDone;},
    {/*interrupted*/},
    [],
    4,
    12,
    false,
    false
    ] call BIS_fnc_holdActionAdd;
};