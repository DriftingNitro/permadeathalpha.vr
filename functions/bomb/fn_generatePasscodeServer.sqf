diag_log "generating passcode";
if (!isServer) exitWith {};
diag_log "is the server";
if (PasscodeGenerated) exitWith {};
diag_log "passcode has not been generated";
passcode = "";
displayPasscode = "";
for "_i" from 0 to 3 do
{
    _random = round (random 9);
    displayPasscode = displayPasscode+(str _random);
    passcode = passcode+(str _random);
};
displayPasscode = displayPasscode+"-";
for "_i" from 4 to 7 do
{
    _random = round (random 9);
    displayPasscode = displayPasscode+(str _random);
    passcode = passcode+(str _random);
};

diag_log format ["passcode is %1",passcode];
publicVariable "passcode";
diag_log format ["display passcode is %1",displayPasscode];
publicVariable "displayPasscode";

diag_log format ["calling to make journal %1",displayPasscode];
[] remoteExecCall ["bomb_fnc_generatePasscodeGlobal", 0, true];
passcodeGenerated = true;
publicVariable "passcodeGenerated";