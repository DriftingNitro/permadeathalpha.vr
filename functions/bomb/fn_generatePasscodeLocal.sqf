diag_log "generating passcode";
if (PasscodeGenerated) exitWith {
    _NotedCode = player createDiaryRecord ["diary", ["Passcode",format ["
    <br/>
    <font size='64'>Passcode
    <br/>
    <font size='58'>%1</font>
    <br/><br/>
    ", displayPasscode]]];
};
diag_log "passcode has not been generated";
passcode = "";
displayPasscode = "";
for "_i" from 0 to 3 do
{
    _random = round (random 9);
    displayPasscode = displayPasscode+(str _random);
    passcode = passcode+(str _random);
};
displayPasscode = displayPasscode+"-";
for "_i" from 4 to 7 do
{
    _random = round (random 9);
    displayPasscode = displayPasscode+(str _random);
    passcode = passcode+(str _random);
};

publicVariable "passcode";
publicVariable "displayPasscode";
passcodeGenerated = true;
publicVariable "passcodeGenerated";

_NotedCode = player createDiaryRecord ["diary", ["Passcode",format ["<br/><font size='64'>Passcode<br/><font size='58'>%1</font><br/><br/>", displayPasscode]]];

openmap [true,false];
["IntelAdded",["Passcode assigned","\A3\Ui_f\data\GUI\Cfg\CommunicationMenu\defend_ca.paa"]] call BIS_fnc_showNotification;