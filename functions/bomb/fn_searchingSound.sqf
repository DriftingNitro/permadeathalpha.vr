_object = _this select 0;
_player = _this select 1;

_player action ["SwitchWeapon", _player, _player, 100];

_soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;

if (typeOf _object == "Land_Laptop_f" ||typeOf _object == "Land_Laptop_unfolded_f") exitWith {
    _filename = selectRandom ["typing1","typing2","typing3","typing4","typing5"];
    _soundToPlay = _soundPath + "sound\"+_filename+".ogg";
    playSound3D [_soundToPlay, _object, false, getPosASL _object, 16,(random [0.9,1,1.1]),18];
};

_filename = selectRandom ["pageturning1","pageturning2","pageturning3","pageturning4"];
_soundToPlay = _soundPath + "sound\"+_filename+".ogg";
playSound3D [_soundToPlay, _object, false, getPosASL _object, 8,(random [0.9,1,1.1]),18];