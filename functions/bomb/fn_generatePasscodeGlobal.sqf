diag_log format ["making journal %1",displayPasscode];

_NotedCode = player createDiaryRecord ["diary", ["Passcode",format ["
<br/>
<font size='64'>Passcode
<br/>
<font size='58'>%1</font>
<br/><br/>
", displayPasscode]]];

["IntelAdded",["Passcode assigned","\A3\Ui_f\data\GUI\Cfg\Hints\Tasks_ca.paa"]] call BIS_fnc_showNotification;