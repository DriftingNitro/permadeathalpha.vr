/* ----------------------------------------------------------------------------

	File: BluforFailure.sqf
	Author: DriftingNitro
	
Description:
    Runs the mission failure sequence for blufor

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */
disableSerialization;
if(!MissionActive) exitWith {diag_log "!!!!!!!!!!IMPORTANT!!!!!!!!!! --!-- MISSION TRYING TO END BUT MISSION HASN'T STARTED";};
diag_log "!!!!!!!!!!IMPORTANT!!!!!!!!!! --!-- MISSION IS ENDING (FAILURE)";
if (EndScreen) exitWith {diag_log "!!!!!!!!!!ERROR!!!!!!!!!! --!-- Mission already OVER";};
EndScreen = true;
playMusic "Failure";
sleep 2;
Command sideRadio "MissionFailed";
["Command", "Mission failed, bring it next time!"] call BIS_fnc_showSubtitle;
sleep 1;
["End2",False,True,False] call BIS_fnc_endMission;