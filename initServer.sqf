/* ----------------------------------------------------------------------------

	File: initServer.sqf
	Author: DriftingNitro
	
Description:
    just hides the mission starter object


Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

missionStarter hideObjectGlobal true;
diag_log "|initServer|--!-- mission starter is hidden";