/* ----------------------------------------------------------------------------

	File: vehicleCreator.sqf
	Author: DriftingNitro
	
Description:
    checks if the unit is the leader unit, then spawns the vehicle for blufor and opfor based on the switch case libaray

Parameter(s):
	- [0] Vehicle : Object
    - [1] Player : Object

Return:
    - none
---------------------------------------------------------------------------- */

/*
O_G_Offroad_01_F		    =	tacs_Offroad_B_Black
CUP_O_UAZ_MG_CHDKZ		    =	tacs_Offroad_B_Armed_Black
CUP_O_GAZ_Vodnik_MedEvac_RU	=	CUP_B_M1133_MEV_Desert
CUP_O_BTR40_MG_TKA		    =	CUP_B_HMMWV_M2_USA
CUP_O_UAZ_Unarmed_CHDKZ	    =	CUP_B_HMMWV_Transport_USA
CUP_O_BTR40_TKA			    =	CUP_B_HMMWV_Unarmed_USA
CUP_O_Ural_CHDKZ		    =	CUP_B_MTVR_USA
*/

_unit = _this select 1;
_nameIs = name _unit;
_classname = typeOf(_this select 0);

hint format ["Hello %1",_nameIs ];
diag_log format ["|VehicleCreator|--!-- %1 is requesting the vehicle spawn",_unit];
sleep 3;

if (creatingVehicle) exitWith {
    hint "A vehicle is already being created"; 
    diag_log format ["|VehicleCreator|--!-- Creating a vehicle already %1",creatingVehicle];
    };

creatingVehicle = true;

if (_unit != Leader_1) exitWith {
    hint "You are not the leader, vehicle spawn rejected";
    diag_log format ["|VehicleCreator|--!-- %1 is rejected request for vehicle spawn",_unit];
    };

diag_log format ["|VehicleCreator|--!-- %1 is granted vehicle spawn",_unit];

hint "spawning your vehicle";
sleep 1;

//Addaction on each vehicle
//_this addaction ["<t color='#FF0000'>Spawn ____</t>", "vehicleCreator.sqf",(typeOf this),6,true,true,"","",6];

diag_log "|VehicleCreator|--!--Checking vehicle library";
private _enemyClassname = switch (_classname) do {
    case "tacs_Offroad_B_Black": {"O_G_Offroad_01_F"};
    case "tacs_Offroad_B_Armed_Black": {"CUP_O_UAZ_MG_CHDKZ"};
    case "CUP_B_M1133_MEV_Desert": {"CUP_O_GAZ_Vodnik_MedEvac_RU"};
    case "CUP_B_HMMWV_M2_USA": {"CUP_O_BTR40_MG_TKA"};
    case "CUP_B_HMMWV_Transport_USA": {"CUP_O_UAZ_Unarmed_CHDKZ"};
    case "CUP_B_HMMWV_Unarmed_USA": {"CUP_O_BTR40_TKA"};
    case "CUP_B_MTVR_USA": {"CUP_O_Ural_CHDKZ"};
    case "B_MRAP_01_F": {"O_MRAP_02_F"};
    case "B_MRAP_01_gmg_F": {"O_MRAP_02_gmg_F"};
    case "B_MRAP_01_hmg_F": {"O_MRAP_02_hmg_F"};
    case "B_G_Offroad_01_repair_F": {"O_G_Offroad_01_repair_F"};
    case "B_G_Offroad_01_F": {"B_G_Offroad_01_F"};
    case "B_G_Offroad_01_armed_F": {"O_G_Offroad_01_armed_F"};
    case "B_Quadbike_01_F": {"O_Quadbike_01_F"};
    case "B_Truck_01_transport_F": {"O_Truck_03_transport_F"};
    case "B_Truck_01_covered_F": {"O_Truck_03_covered_F"};
    case "B_Truck_01_Repair_F": {"O_Truck_03_repair_F"};
    case "B_Truck_01_medical_F": {"O_Truck_03_medical_F"};
    case "B_Truck_01_ammo_F": {"O_Truck_03_ammo_F"};
    case "B_Truck_01_fuel_F": {"O_Truck_03_fuel_F"};
    case "B_G_Van_01_transport_F": {"O_G_Van_01_transport_F"};
    case "B_G_Van_01_fuel_F": {"O_G_Van_01_fuel_F"};
    case "B_APC_Tracked_01_rcws_F": {"O_APC_Wheeled_02_rcws_F"};
    case "B_UGV_01_F": {"O_UGV_01_F"};
    case "B_UGV_01_rcws_F": {"O_UGV_01_rcws_F"};
    case "B_APC_Tracked_01_AA_F": {"O_APC_Tracked_02_AA_F"};
    case "B_APC_Wheeled_01_cannon_F": {"O_APC_Tracked_02_cannon_F"};
    case "B_MBT_01_cannon_F": {"O_MBT_02_cannon_F"};
    case "B_MBT_01_arty_F": {"O_MBT_02_arty_F"};
    case "B_Boat_Armed_01_minigun_F": {"O_Boat_Armed_01_hmg_F"};
    case "B_SDV_01_F": {"O_SDV_01_F"};
    case "B_Lifeboat": {"O_Lifeboat"};
    case "B_Boat_Transport_01_F": {"O_Boat_Transport_01_F"};
    case "B_G_Boat_Transport_01_F": {"O_G_Boat_Transport_01_F"};
    case "B_Heli_Transport_01_F": {"O_Heli_Light_02_v2_F"};
    case "B_Heli_Attack_01_F": {"O_Heli_Attack_02_black_F"};
    case "B_Heli_Light_01_F": {"O_Heli_Light_02_unarmed_F"};
    case "B_Heli_Light_01_armed_F": {"O_Heli_Light_02_v2_F"};
    case "B_Mortar_01_F": {"O_Mortar_01_F"};
    case "B_G_Mortar_01_F": {"O_G_Mortar_01_F"};
    case "B_UAV_02_F": {"O_UAV_02_F"};
    case "B_UAV_02_CAS_F": {"O_UAV_02_CAS_F"};
    default {_classname};
};

if (_classname == _enemyClassname) then 
{
    _error = parseText format["<t size='1.5' font='Zeppelin33'>Classname</t><br/><br/><t size='0.75' font='LucidaConsoleB' color='#ff0000'>%1</t><br/><br/>is not available in script, edit <br/><br/><t size='0.75' font='LucidaConsoleB'>vehicleCreator.sqf</t><br/><br/>Identical vehicle has been spawned on the enemy side instead.",_classname]; 
    hint _error;
    sleep 10;
};

_index = _this select 3;
_Opveh = createVehicle [_enemyClassname, getMarkerPos "East_Start", [], 5, "NONE"];
diag_log format ["|VehicleCreator|--!-- %1 spawned on the opfor side",_enemyClassname];

_veh = createVehicle [_classname, getMarkerPos "West_start", [], 5, "NONE"];
diag_log format ["|VehicleCreator|--!-- %1 spawned on the blufor side",_classname];

_Opveh allowDamage false;
_veh allowDamage false;
diag_log "Vehicles indestructible";

_veh setDir (markerDir "Blufor_VehicleSpawner");
diag_log "|VehicleCreator|--!--Blufor vehicle direction set";
_Opveh setDir (markerDir "Opfor_VehicleSpawner");
diag_log "|VehicleCreator|--!--Opfor vehicle direction set";

sleep 1;

clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearItemCargoGlobal _veh;
clearBackpackCargoGlobal _veh;

clearWeaponCargoGlobal _Opveh;
clearMagazineCargoGlobal _Opveh;
clearItemCargoGlobal _Opveh;
clearBackpackCargoGlobal _Opveh;
diag_log "|VehicleCreator|--!--Vehicles cargo cleared";

_name = getText (configFile >> "cfgVehicles" >> typeOf _veh >> "displayName");
_Opname = getText (configFile >> "cfgVehicles" >> typeOf _Opveh >> "displayName");
diag_log "|VehicleCreator|--!--classnames gathered";

hint format ["Blufor have spawned a %1, and Opfor have received a %2", _name, _Opname ];

_Opveh allowDamage true;
_veh allowDamage true;
diag_log "|VehicleCreator|--!--Vehicles destructible";

zMod1 addCuratorEditableObjects [[_veh],true ];
zMod1 addCuratorEditableObjects [[_Opveh],true ];
zMod2 addCuratorEditableObjects [[_veh],true ];
zMod2 addCuratorEditableObjects [[_Opveh],true ];
zMod3 addCuratorEditableObjects [[_veh],true ];
zMod3 addCuratorEditableObjects [[_Opveh],true ];
diag_log "|VehicleCreator|--!--Vehicles added to curators";

sleep 2;
_Opveh lock 3;
creatingVehicle = false;
diag_log "|VehicleCreator|--!--Vehicles allowed to be spawned again";