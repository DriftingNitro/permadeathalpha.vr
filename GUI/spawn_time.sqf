/* ----------------------------------------------------------------------------

	File: spawn_time.sqf
	Author: DriftingNitro
	
Description:
    create the time set diolag and disable the esc key

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

_handle=createdialog "time_set";
diag_log "|spawnTime|--!-- spawn time setting diolag";
(findDisplay 11) displaySetEventHandler ["KeyDown","if((_this select 1)isEqualTo 1) then {true}"];        //Disable ESC
diag_log "|spawnTime|--!-- esc button disabled";