/* ----------------------------------------------------------------------------

	File: tool_confirm.sqf
	Author: DriftingNitro
	
Description:
    Checks if all the necessary information for the mission configuration has been
    entered and begins the process for notifying the players that paramters are set

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

if(!enteredWest) exitWith {    
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= "The Blufor starting position has not been set";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
};

diag_log "|toolConfirm|--!-- West point confirmed";

if(!enteredEast) exitWith {
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= "The Opfor starting position has not been set";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
};

diag_log "|toolConfirm|--!-- East point confirmed";

if(!enteredObj) exitWith {
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= "The objective marker position has not been set";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
};

diag_log "|toolConfirm|--!-- Objective marker position confirmed";

if(!enteredTime) exitWith {
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= "Time limit has not been set";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
};

diag_log format ["|toolConfirm|--!-- Time entered and ready %1",END_TIME];

[[],"GUI\notification_confirm.sqf"] remoteExec ["execVM",0,true];

diag_log "|toolConfirm|--!-- Notification of confirmation should be globally sent";
closeDialog 0;
diag_log "|toolConfirm|--!-- Close menu";
[true,true] call BIS_fnc_forceCuratorInterface;
diag_log "|toolConfirm|--!-- For zeus, force interface";