/* ----------------------------------------------------------------------------

	File: tool_WestStart.sqf
	Author: DriftingNitro
	
Description:
    Prompts the player to select the start location of the blufor

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

if (!local player) exitWith {};
openMap [true, true];

disableSerialization;
diag_log "|toolWest|--!-- starting west start point selection";

"H8erHUD" cutRsc ["H8erHUD","PLAIN"];

_string="Set the Blufor spawn position";

waitUntil {!isNull (uiNameSpace getVariable "H8erHUD")};
	_display = uiNameSpace getVariable "H8erHUD";
	_setText = _display displayCtrl 1001;
	_setText ctrlSetStructuredText (parseText format ["%1",_string]);
	_setText ctrlSetBackgroundColor [0,0,0,0];

clicked = false;
onMapSingleClick "'west_start' setMarkerPos _pos; clicked=true;"; 
waituntil {clicked};
onMapSingleClick ""; 
"H8erHUD" cutRsc ["default","PLAIN"];
diag_log "|toolWest|--!-- position selected and moving flag";
BluforFlag setPos (getMarkerPos "west_start");
openMap [false, false];
enteredWest = true;
diag_log "|toolWest|--!-- West entry true, Opening up menu again";
null = execVM "GUI\spawn_menu.sqf"