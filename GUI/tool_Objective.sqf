
/* ----------------------------------------------------------------------------

	File: tool_Objective.sqf
	Author: DriftingNitro
	
Description:
    Prompts the player to select the location of the objective item (i.e. the tower with the bomb in it)

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */
if (!local player) exitWith {};
openMap [true, true];

disableSerialization;

4 cutRsc ["H8erHUD","PLAIN"];

_string="Set the objective marker location";

waitUntil {!isNull (uiNameSpace getVariable "H8erHUD")};
	_display = uiNameSpace getVariable "H8erHUD";
	_setText = _display displayCtrl 1001;
	_setText ctrlSetStructuredText (parseText format ["%1",_string]);
	_setText ctrlSetBackgroundColor [0,0,0,0];

clicked = false;
onMapSingleClick "'Objective' setMarkerPos _pos; clicked=true; tower setVehiclePosition [_pos, [], 0, 'none'];"; 
waituntil {clicked};
onMapSingleClick ""; 
4 cutRsc ["default","PLAIN"];

_posTower = AGLToASL (tower modelToWorld [-3.2,-3.0,2.467]);
[terminal, 180] remoteExec ["setDir", 0, false];
//[terminal, _posTower] remoteExec ["setPosASL", 0, false];

openMap [false, false];
enteredObj = true;
null = execVM "GUI\spawn_menu.sqf"