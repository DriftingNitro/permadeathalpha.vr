/* ----------------------------------------------------------------------------

	File: update_opfor.sqf
	Author: DriftingNitro
	
Description:
    changes the text of the remaining blufor display

Parameter(s):
	- [0] Integer : Number

Return:
    - none
---------------------------------------------------------------------------- */
if (hasInterface) then {
		
	disableSerialization;
	diag_log format ["|updateBlufor|--!-- attempting to get passed initMission_done %1",initMission_done];
	waitUntil {initMission_done};
	diag_log "|updateBlufor|--!-- boolean check passed";
	private _BluDisplayNumber =_this select 0;
	diag_log format ["|updateBlufor|--!-- %1 is the remaining number of opfor, updated on interface",_BluDisplayNumber];
	"NitroBlu" cutRsc ["NitroBlu","PLAIN"];
	_display = uiNameSpace getVariable "NitroBlu";
	_setText = _display displayCtrl 1015;
	((uiNameSpace getVariable "NitroBlu") displayCtrl 1015) ctrlSetStructuredText (parseText format ["<t size='2' color='#006296'>%1 | BLUFOR</t>",_BluDisplayNumber]);
};