/* ----------------------------------------------------------------------------

	File: tool_timeCheck.sqf
	Author: DriftingNitro
	
Description:
    checks if the number entered is valid and sets it globally

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

Timer_Length = parseNumber END_TIME;

if((Timer_Length < 1201) && (Timer_Length > 59)) then {
   // playSound "";
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= format ["Time limit set to %1 seconds",Timer_Length];
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];

        closeDialog 0; 
        _nil=[]ExecVM 'GUI\spawn_menu.sqf';
    publicVariable "END_TIME";
    enteredTime = true;
} else {
    //playSound "";
    disableSerialization;
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= format ["%1 is invalid, enter an integer between 60 and 1200",END_TIME];
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
    enteredTime = false;
    
};