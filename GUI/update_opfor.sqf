/* ----------------------------------------------------------------------------

	File: update_opfor.sqf
	Author: DriftingNitro
	
Description:
    changes the text of the remaining opfor display

Parameter(s):
	- [0] Integer : Number

Return:
    - none
---------------------------------------------------------------------------- */
if (hasInterface) then {

	disableSerialization;
	diag_log format ["|updateOpfor|--!-- attempting to get passed initMission_done %1",initMission_done];
	waitUntil {initMission_done};
	diag_log "|updateOpfor|--!-- boolean check passed";
	private _OpDisplayNumber =_this select 0;
	diag_log format ["|updateOpfor|--!-- %1 is the remaining number of opfor, updated on interface",_OpDisplayNumber];
	"NitroOp" cutRsc ["NitroOp","PLAIN"];
	_display = uiNameSpace getVariable "NitroOp";
	_setText = _display displayCtrl 1020;
	((uiNameSpace getVariable "NitroOp") displayCtrl 1020) ctrlSetStructuredText (parseText format ["<t size='2' color='#960000'>OPFOR | %1</t>",_OpDisplayNumber]);
};