
class bomb
{
	idd=9005;
	movingenable=true;
	class controls
	{
		class bomb_frame: RlcFrame
		{
			idc = 1830;
			x = 0.432968 * safezoneW + safezoneX;
			y = 0.313 * safezoneH + safezoneY;
			w = 0.134062 * safezoneW;
			h = 0.396 * safezoneH;
		};
		class bomb_back: IGUIBack
		{
			idc = 2200;
			x = 0.432968 * safezoneW + safezoneX;
			y = 0.313 * safezoneH + safezoneY;
			w = 0.134062 * safezoneW;
			h = 0.396 * safezoneH;
		};
		class button_1: RlcButton
		{
			idc = 1621;
			text = "1"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[1]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_2: RlcButton
		{
			idc = 1620;
			text = "2"; //--- ToDo: Localize;
			x = 0.484531 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[2]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_3: RlcButton
		{
			idc = 1622;
			text = "3"; //--- ToDo: Localize;
			x = 0.525781 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[3]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_6: RlcButton
		{
			idc = 1623;
			text = "6"; //--- ToDo: Localize;
			x = 0.525781 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[6]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_5: RlcButton
		{
			idc = 1624;
			text = "5"; //--- ToDo: Localize;
			x = 0.484531 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[5]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_4: RlcButton
		{
			idc = 1625;
			text = "4"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[4]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_9: RlcButton
		{
			idc = 1626;
			text = "9"; //--- ToDo: Localize;
			x = 0.525781 * safezoneW + safezoneX;
			y = 0.555 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[9]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_8: RlcButton
		{
			idc = 1627;
			text = "8"; //--- ToDo: Localize;
			x = 0.484531 * safezoneW + safezoneX;
			y = 0.555 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[8]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_7: RlcButton
		{
			idc = 1628;
			text = "7"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.555 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[7]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_reset: RlcButton
		{
			idc = 1629;
			text = "Reset"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.632 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[10]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_0: RlcButton
		{
			idc = 1630;
			text = "0"; //--- ToDo: Localize;
			x = 0.484531 * safezoneW + safezoneX;
			y = 0.632 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[0]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class button_confirm: RlcButton
		{
			idc = 1631;
			text = "Confirm"; //--- ToDo: Localize;
			x = 0.525781 * safezoneW + safezoneX;
			y = 0.632 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.055 * safezoneH;
			action = "_nil=[11]ExecVM ""BombGUI\bomb_input.sqf""";
			tooltip = "";
		};
		class text_output: RmcText
		{
			idc = 1030;
			text = "Passcode"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.335 * safezoneH + safezoneY;
			w = 0.113437 * safezoneW;
			h = 0.055 * safezoneH;
			colorBackground[] = {-1,-1,-1,1};

		};
	};
};

class tools
{
	idd=9002;
	movingenable=false;
	class controls
	{
		class tools_Back: RlcPicture
		{
			idc = 1200;
			text = "GUI\background.paa";
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.55 * safezoneH;
		};
		class tools_Frame: RlcFrame
		{
			idc = 1800;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.55 * safezoneH;
		};
		class tools_West: RlcButton
		{
			idc = 1600;
			text = "Set West Start";
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.066 * safezoneH;
			action = "closeDialog 0;_nil=[]ExecVM ""GUI\tool_WestStart.sqf""";
			tooltip = "Set the position of where Blufor will spawn when they start the mission";
		};
		class tools_East: RlcButton
		{
			idc = 1601;
			text = "Set East Start";
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.066 * safezoneH;
			action = "closeDialog 0;_nil=[]ExecVM ""GUI\tool_EastStart.sqf""";
			tooltip = "Set the position of where Opfor will spawn when the mission starts";
		};
		class tools_Objective: RlcButton
		{
			idc = 1602;
			text = "Set Objective Marker";
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.621 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.066 * safezoneH;
			action = "closeDialog 0;_nil=[]ExecVM ""GUI\tool_Objective.sqf""";
			tooltip = "Set the position of the Objective marker for the players to assault";
		};
		class tools_Time: RlcButton
		{
			idc = 1603;
			text = "Set Time Limit";
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.066 * safezoneH;
			action = "closeDialog 0;_nil=[]ExecVM 'GUI\spawn_time.sqf'";
			tooltip = "Set the time limit, countdown starts when players activate the mission";
		};
		class tools_done: RlcButton
		{
			idc = 1606;
			text = "Confirm Done";
			x = 0.572189 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.0928125 * safezoneW;
			h = 0.287 * safezoneH;
			action = "_nil=[]ExecVM ""GUI\tool_confirm.sqf""";
			tooltip = "Confirm mission parameters and allow players to start the mission when they're ready";
		};
	};
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by D.Nitro, v1.063, #Tehato)
////////////////////////////////////////////////////////
class time_set
{
	idd=11;
	movingenable=false;
	class controls
	{
		class time_frame: RlcFrame
		{
			idc = 2800;
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.11 * safezoneH;
		};
		class time_back: IGUIBack
		{
			idc = 2801;
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.11 * safezoneH;
		};
		class time_ok: RlcButton
		{
			idc = 2802;
			x = 0.55 * safezoneW + safezoneX;
			y = 0.47 * safezoneH + safezoneY;
			w = 0.033 * safezoneW;
			h = 0.040 * safezoneH;
			text = "Confirm";
			action = "missionNameSpace setVariable ['END_TIME', (ctrlText 2803), true]; _nil=[]ExecVM 'GUI\tool_timeCheck.sqf'";
		};
		class time_edit: RscEdit
		{
			idc = 2803;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.489 * safezoneH + safezoneY;
			w = 0.113437 * safezoneW;
			h = 0.022 * safezoneH;
			text = "";
			tooltip = "Enter an integer between 60 and 12000";
		};
		class time_text: RscText
		{
			idc = 2804;
			text = "Type in the time limit (in seconds)";
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.434 * safezoneH + safezoneY;
			w = 0.15 * safezoneW;
			h = 0.044 * safezoneH;
		};
	};
};

class RscTitles
{
	class Default 
	{
		idd = -1;
		fadein = 0;
		fadeout = 0;
		duration = 0;
	};
	
	class infoNotification
	{
		idd = 10022;
		movingEnable =  0;
		enableSimulation = 1;
		enableDisplay = 1;
		duration     =  10;
		fadein       =  1;
		fadeout      =  2;
		name = "infoNotification";
		onLoad = "with uiNameSpace do { infoNotification = _this select 0 }";
		class controls 
		{
			class structuredText
			{
				access = 0;
				type = 13;
				idc = 1012;
				style = 0x00;
				lineSpacing = 1;
				x = 0.319531 * safezoneW + safezoneX;
				y = 0.126 * safezoneH + safezoneY;
				w = 0.391875 * safezoneW;
				h = 0.166 * safezoneH;
				size = 0.015;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,1,1,1};
				text = "";
				font = "PuristaSemiBold";
				class Attributes
				{
					font = "PuristaSemiBold";
					color = "#FFFFFF";
					align = "CENTER";
					valign = "top";
					shadow = true;
					shadowColor = "#000000";
					underline = false;
					size = "4";
				}; 
			};
		};
	};
	class H8erHUD
	{
		idd = 10000;
		movingEnable =  0;
		enableSimulation = 1;
		enableDisplay = 1;
		duration     =  99999;
		fadein       =  0.1;
		fadeout      =  2;
		name = "H8erHUD";
		onLoad = "with uiNameSpace do { H8erHUD = _this select 0 }";
		class controls 
		{
			class structuredText
			{
				access = 0;
				type = 13;
				idc = 1001;
				style = 0x00;
				lineSpacing = 1;
				x = 0.304062 * safezoneW + safezoneX;
				y = 0.236 * safezoneH + safezoneY;
				w = 0.391875 * safezoneW;
				h = 0.166 * safezoneH;
				size = 0.020;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,1,1,1};
				text = "";
				font = "PuristaSemiBold";
				class Attributes
				{
					font = "PuristaSemiBold";
					color = "#FFFFFF";
					align = "CENTER";
					valign = "top";
					shadow = true;
					shadowColor = "#000000";
					underline = false;
					size = "4";
				}; 
			};
		};
	};
	class NitroInfo
	{
		idd = 10010;
		movingEnable =  0;
		enableSimulation = 1;
		enableDisplay = 1;
		duration     =  99999;
		fadein       =  3;
		fadeout      =  3;
		name = "NitroInfo";
		onLoad = "with uiNameSpace do {NitroInfo = _this select 0}";
		class controls 
		{
			class structuredText
			{
				access = 0;
				type = 13;
				idc = 1010;
				style = 0x00;
				lineSpacing = 1;
				x = 0.45 * safezoneW + safezoneX;
				y = 0.038 * safezoneH + safezoneY;
				w = 0.25;
				h = 0.1;
				size = 0.020;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,1,1,1};
				text = "";
				font = "PuristaSemiBold";
				class Attributes
				{
					font = "PuristaSemiBold";
					color = "#FFFFFF";
					align = "Left";
					valign = "top";
					shadow = true;
					shadowColor = "#000000";
					underline = false;
					size = "4";
				}; 
			};
		};
	};
	class NitroBlu
	{
		idd = 2020;
		movingEnable =  0;
		enableSimulation = 1;
		enableDisplay = 1;
		duration     =  99999;
		fadein       =  0;
		fadeout      =  0;
		name = "NitroBlu";
		onLoad = "with uiNameSpace do { NitroBlu = _this select 0 }";
		class controls 
		{
			class structuredText
			{
				access = 0;
				type = 13;
				idc = 1015;
				style = 0x00;
				lineSpacing = 1;
				x = 0.36 * safezoneW + safezoneX;
				y = 0.050 * safezoneH + safezoneY;
				w = 0.2;
				h = 0.1;
				size = 0.020;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,1,1,1};
				text = "";
				font = "PuristaSemiBold";
				class Attributes
				{
					font = "PuristaSemiBold";
					color = "#FFFFFF";
					align = "right";
					valign = "top";
					shadow = true;
					shadowColor = "#000000";
					underline = false;
					size = "4";
				}; 
			};
		};
	};
	class NitroOp
	{
		idd = 2030;
		movingEnable =  0;
		enableSimulation = 1;
		enableDisplay = 1;
		duration     =  99999;
		fadein       =  0;
		fadeout      =  0;
		name = "NitroOp";
		onLoad = "with uiNameSpace do { NitroOp = _this select 0 }";
		class controls 
		{
			class structuredText
			{
				access = 0;
				type = 13;
				idc = 1020;
				style = 0x00;
				lineSpacing = 1;
				x = 0.56 * safezoneW + safezoneX;
				y = 0.050 * safezoneH + safezoneY;
				w = 0.2;
				h = 0.1;
				size = 0.020;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,1,1,1};
				text = "";
				font = "PuristaSemiBold";
				class Attributes
				{
					font = "PuristaSemiBold";
					color = "#FFFFFF";
					align = "Left";
					valign = "top";
					shadow = true;
					shadowColor = "#000000";
					underline = false;
					size = "4";
				}; 
			};
		};
	};
};