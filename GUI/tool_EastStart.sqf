/* ----------------------------------------------------------------------------

	File: tool_EastStart.sqf
	Author: DriftingNitro
	
Description:
    Prompts the player to select the start location of the opfor

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

if (!local player) exitWith {};
openMap [true, true];

disableSerialization;
diag_log "|toolEast|--!-- starting east start point selection";

"H8erHUD" cutRsc ["H8erHUD","PLAIN"];

_string="Set the Opfor spawn position";

waitUntil {!isNull (uiNameSpace getVariable "H8erHUD")};
	_display = uiNameSpace getVariable "H8erHUD";
	_setText = _display displayCtrl 1001;
	_setText ctrlSetStructuredText (parseText format ["%1",_string]);
	_setText ctrlSetBackgroundColor [0,0,0,0];

clicked = false;
onMapSingleClick "'east_start' setMarkerPos _pos; clicked=true;"; 
waituntil {clicked};
onMapSingleClick ""; 
"H8erHUD" cutRsc ["default","PLAIN"];
diag_log "|toolEast|--!-- position selected and moving flag";
OpforFlag setPos (getMarkerPos "east_start");
openMap [false, false];
enteredEast = true;
diag_log "|toolEast|--!-- East entry true, Opening up menu again";
null = execVM "GUI\spawn_menu.sqf"
