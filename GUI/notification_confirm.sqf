/* ----------------------------------------------------------------------------

	File: tool_WestStart.sqf
	Author: DriftingNitro
	
Description:
    Generates the notification for the players telling them that the parameters have been set
    both visually an audibly. Also unhiding the vehicle spawners and the mission starter booth

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

vehicleSpawner_0 hideObjectGlobal false;
vehicleSpawner_1 hideObjectGlobal false;
vehicleSpawner_2 hideObjectGlobal false;
vehicleSpawner_3 hideObjectGlobal false;
vehicleSpawner_4 hideObjectGlobal false;
vehicleSpawner_5 hideObjectGlobal false;
vehicleSpawner_6 hideObjectGlobal false;
diag_log "|notifConfirm|--!-- Mission starter and vehicle spawners should be visible";

uiSleep 30;
missionStarter hideObjectGlobal false;
arrowStarter hideObjectGlobal false;
if (isServer) then {
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping01.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping02.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping03.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping04.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping05.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping06.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
    sleep 0.2;
    playSound3D ["a3\Ui_F_Curator\Data\Sound\CfgSound\ping07.wss", missionStarter, false, getPosASL missionStarter, 8,1,500];
};

if (hasInterface) then {
    diag_log "|notifConfirm|--!-- running on all non-dedicated server clients, spawning text notification that mission maker is ready";

    disableSerialization;
    closeDialog 0; 
    "infoNotification" cutRsc ["default","PLAIN"];

        "infoNotification" cutRsc ["infoNotification","PLAIN"];
        _stringWorked= "The mission parameters have been set, ready to begin";
        waitUntil {!isNull (uiNameSpace getVariable "infoNotification")};
            _display = uiNameSpace getVariable "infoNotification";
            _setText = _display displayCtrl 1012;
            _setText ctrlSetStructuredText (parseText format ["%1",_stringWorked]);
            _setText ctrlSetBackgroundColor [0,0,0,0];
            diag_log "|notifConfirm|--!-- Waiting until mission is activated by the leader";
            waitUntil {MissionActive};
    diag_log "|notifConfirm|--!-- Mission is now activated and removing notification";
    "infoNotification" cutRsc ["default","PLAIN"];  
};
