/* ----------------------------------------------------------------------------

	File: tool_WestStart.sqf
	Author: DriftingNitro
	
Description:
    Spawns the mission setup menu for the missions maker to configure the mission to their settings

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */

_handle=createdialog "tools";
diag_log "|spawnMenu|--!-- spawn main menu diolag";
(findDisplay 9002) displaySetEventHandler ["KeyDown","if((_this select 1)isEqualTo 1) then {true}"];        //Disable ESC
diag_log "|spawnMenu|--!-- esc button disabled";