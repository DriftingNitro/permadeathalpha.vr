/* ----------------------------------------------------------------------------

	File: BluforVictory.sqf
	Author: DriftingNitro
	
Description:
    Runs the mission victory sequence for blufor

Parameter(s):
	- none

Return:
    - none
---------------------------------------------------------------------------- */
disableSerialization;
if(!MissionActive) exitWith {diag_log "!!!!!!!!!!IMPORTANT!!!!!!!!!! --!-- MISSION TRYING TO END BUT MISSION HASN'T STARTED";};
diag_log "!!!!!!!!!!IMPORTANT!!!!!!!!!! --!-- MISSION IS ENDING (VICTORY)";
if (VictoryPlaying) exitWith {diag_log "!!!!!!!!!!ERROR!!!!!!!!!! --!-- Mission already OVER (victory)";};
if (EndScreen) exitWith {diag_log "!!!!!!!!!!ERROR!!!!!!!!!! --!-- Mission already OVER (failure)";};
EndScreen = true;
VictoryPlaying = true;
sleep 1;
playMusic "Victory";
sleep 2;
Command sideRadio "MissionAccomplished";
["Command", "Mission Accomplished! Well done!"] call BIS_fnc_showSubtitle;
sleep 2;
["End1",True,True,False] call BIS_fnc_endMission;