/* ----------------------------------------------------------------------------

	File: initMission_local.sqf
	Author: DriftingNitro
	
Description:
    checks if the unit requesting to start the mission is the leader unit
    script started from the action on the phone booth that activates the mission

Parameter(s):
	-  [1] Player : Object

Return:
    - none
---------------------------------------------------------------------------- */

_unit = _this select 1;

diag_log "|initMission_Local|--!--Testing if leader is asking to start the mission";
if (_unit != Leader_1) exitWith {hint "You are not the leader, mission must be initiated by the leader.";};
diag_log "|initMission_Local|--!--Test passed and continuing the startup (1)";

diag_log "|initMission_Local|--!--Testing if mission is already activating";
if (MissionActive) exitWith {hint "Its already on"};
diag_log "|initMission_Local|--!--Test passed and continuing the startup (2)";

MissionActive = true;
publicVariable "MissionActive";
diag_log "|initMission_Local|--!--Calling initMission globally";
if (isMultiplayer) then {
	[[],"initMission.sqf"] remoteExec ["execVM",0,false];
	initTime = serverTime;
	publicVariable "initTime";
}
else
{
	[[],"initMission_sp.sqf"] remoteExec ["execVM",0,false];
	initTime = diag_tickTime;
	publicVariable "initTime";
};
waitUntil {beginPreload};
diag_log "|initMission_Local|--!--Calling missionStart_fnc_getUnits, to the SERVER ONLY";
[] remoteExecCall ["missionStarter_fnc_getUnits", 2, false];